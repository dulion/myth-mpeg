package com.dulion.myth.mpeg.service.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

public class EpisodeList {

    @JsonProperty("title")
    private String _title;

    @JsonProperty("episodes")
    private List<Episode> _episodes;

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public List<Episode> getEpisodes() {
        if (null == _episodes) {
            _episodes = Lists.newArrayList();
        }
        return _episodes;
    }
}
