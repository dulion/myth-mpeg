package com.dulion.myth.mpeg.service.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.config.RegisterConstructorMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;

import com.dulion.myth.mpeg.service.api.Episode;
import com.dulion.myth.mpeg.service.api.Title;

public interface MythDAO {

    @SqlQuery("select title, count(title) as episodes from recordedprogram group by title")
    @RegisterBeanMapper(Title.class)
    List<Title> titles();

    @SqlQuery("select subtitle, chanid, starttime from recordedprogram where title=:title")
    @RegisterConstructorMapper(Episode.class)
    List<Episode> episodes(@Bind("title") String title);

}
