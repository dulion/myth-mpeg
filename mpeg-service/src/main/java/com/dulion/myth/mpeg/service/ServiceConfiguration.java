package com.dulion.myth.mpeg.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

public class ServiceConfiguration extends Configuration {

    @Valid
    @NotNull
    @JsonProperty("database")
    private final DataSourceFactory _database = new DataSourceFactory();

    public DataSourceFactory getDatabase() {
        return _database;
    }

}
