package com.dulion.myth.mpeg.service.api;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Recording {

    @NotEmpty
    @JsonProperty("key")
    private String _key;

    @NotEmpty
    @JsonProperty("title")
    private String _title;

    public String getKey() {
        return _key;
    }

    public void setKey(String key) {
        _key = key;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

}
