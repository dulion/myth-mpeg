package com.dulion.myth.mpeg.service;

import org.jdbi.v3.core.Jdbi;

import com.dulion.myth.mpeg.service.api.MythService;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.dropwizard.Application;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class ServiceApplication extends Application<ServiceConfiguration> {

    @Override
    public String getName() {
        return "myth-mpeg-service";
    }

    @Override
    public void initialize(Bootstrap<ServiceConfiguration> bootstrap) {
        ObjectMapper mapper = bootstrap.getObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.setSerializationInclusion(Include.NON_DEFAULT);
        mapper.disable(
            MapperFeature.AUTO_DETECT_CREATORS,
            MapperFeature.AUTO_DETECT_FIELDS,
            MapperFeature.AUTO_DETECT_GETTERS,
            MapperFeature.AUTO_DETECT_IS_GETTERS,
            MapperFeature.AUTO_DETECT_SETTERS);

    }

    @Override
    public void run(ServiceConfiguration configuration, Environment environment) throws Exception {
        JdbiFactory factory = new JdbiFactory();
        Jdbi jdbi = factory.build(environment, configuration.getDatabase(), "postgresql");

        final MythService resource = new MythService(jdbi);
        final ServiceHealthCheck healthCheck = new ServiceHealthCheck();

        environment.healthChecks().register("service", healthCheck);
        environment.jersey().register(resource);

    }

    public static void main(String[] arguments) throws Exception {
        new ServiceApplication().run(arguments);
    }
}
