package com.dulion.myth.mpeg.service.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

public class TitleList {

    @JsonProperty("titles")
    private List<Title> _titles;

    public List<Title> getRecordings() {
        if (null == _titles) {
            _titles = Lists.newArrayList();
        }
        return _titles;
    }
}
