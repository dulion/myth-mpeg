package com.dulion.myth.mpeg.service.api;

import java.beans.ConstructorProperties;
import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Episode {

    @JsonProperty("chanid")
    private int _chanid;

    @JsonProperty("start")
    private Instant _start;

    @JsonProperty("subtitle")
    private String _subtitle;

    public Episode() {
    }

    @ConstructorProperties({ "chanid", "starttime", "subtitle" })
    public Episode(int chanid, Instant start, String subtitle) {
        _chanid = chanid;
        _start = start;
        _subtitle = subtitle;
    }

    public int getChanid() {
        return _chanid;
    }

    public void setChanid(int chanid) {
        _chanid = chanid;
    }

    public Instant getStart() {
        return _start;
    }

    public void setStart(Instant start) {
        _start = start;
    }

    public String getSubtitle() {
        return _subtitle;
    }

    public void setSubtitle(String subtitle) {
        _subtitle = subtitle;
    }

}
