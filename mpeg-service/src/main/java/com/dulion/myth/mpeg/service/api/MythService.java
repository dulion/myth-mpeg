package com.dulion.myth.mpeg.service.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.dulion.myth.mpeg.service.dao.MythDAO;

@Path("/titles")
@Produces(MediaType.APPLICATION_JSON)
public class MythService {

    private static final Logger LOG = LoggerFactory.getLogger(MythService.class);

    private final Jdbi _jdbi;

    public MythService(Jdbi jdbi) {
        _jdbi = jdbi;
    }

    @GET
    @Timed
    public TitleList getTitleList() {
        TitleList list = new TitleList();
        list.getRecordings().addAll(_jdbi.withHandle(handle -> handle.attach(MythDAO.class).titles()));
        return list;
    }

    @GET
    @Path("{title}/episodes")
    @Timed
    public EpisodeList getEpisodeList(@PathParam("title") String title) {
        LOG.info(title);
        EpisodeList list = new EpisodeList();
        list.setTitle(title);
        list.getEpisodes().addAll(_jdbi.withHandle(handle -> handle.attach(MythDAO.class).episodes(title)));
        return list;
    }
}
