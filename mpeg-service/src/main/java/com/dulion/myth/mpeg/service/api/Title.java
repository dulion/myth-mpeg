package com.dulion.myth.mpeg.service.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Title {

    @JsonProperty("title")
    private String _title;

    @JsonProperty("episodes")
    private int _episodes;

    public Title() {
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public int getEpisodes() {
        return _episodes;
    }

    public void setEpisodes(int episodes) {
        _episodes = episodes;
    }
}
